# -*- coding: utf-8 -*-

# Cotinga helps maths teachers creating worksheets
# and managing pupils' progression.
# Copyright 2018-2022 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Cotinga.

# Cotinga is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Cotinga is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Cotinga; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


# ------------------------------------NOTE------------------------------------
# THIS SETUP.PY FILE IS NECESSARY TO BUILD PACKAGE ON LAUNCHPAD.NET
# AS LONG AS THE BUILD PROCESS DOES NOT TAKE PYPROJECT.TOML INTO ACCOUNT
# THEREFORE IT IS WRITTEN DEPENDING ON VALUES TAKEN FROM PYPROJECT.TOML
# WHEREVER POSSIBLE.
# ----------------------------------------------------------------------------

from pathlib import Path
from shutil import copyfile
from setuptools import setup, find_packages

import toml

pp_path = Path(__file__).parent / 'pyproject.toml'
data_path = Path(__file__).parent / 'cotinga/data'
copyfile(pp_path, data_path / 'pyproject.toml')
print('[setup.py] Copied pyproject.toml to cotinga/data/')
rd_path = Path.home() / '.local/share/cotinga/run/doc'
if not rd_path.is_dir():
    rd_path.mkdir(parents=True, exist_ok=True)
    print(f'[setup.py] Created {rd_path}')


with open(pp_path, 'r') as f:
    pp = toml.load(f)

metadata = pp['tool']['poetry']
dep = metadata['dev-dependencies']
excluded = ['*tests', '*.pytest_cache', '*.venv', '*.vscode',
            '*cotinga.build', '*cotinga.egg-info', '*dist', '*toolbox']

setup(
    name=metadata['name'],
    version=metadata['version'],
    url='https://gitlab.com/nicolas.hainaux/cotinga',
    license='GNU General Public License v3 or later (GPLv3+)',
    author=', '.join(metadata['authors']),
    tests_require=list(dep.keys()),
    install_requires=[f'{k}{dep[k].replace("^", ">=")}' for k in dep],
    author_email='<nh.techn@posteo.net>',
    description=metadata['description'],
    entry_points={'gui_scripts': ['cotinga = cotinga:run']},
    long_description='\n'.join([Path('README.rst').read_text(),
                                Path('CHANGELOG.rst').read_text()]),
    packages=find_packages(exclude=excluded),
    include_package_data=True,
    platforms=['Linux'],
    test_suite='tests',
    classifiers=['Programming Language :: Python',
                 'Programming Language :: Python :: 3.8',
                 'License :: OSI Approved :: '
                 'GNU General Public License v3 or later (GPLv3+)']
        + metadata['classifiers']
)
