

Overview
========

Cotinga helps teachers to manage their pupils' progression.

Documentation
=============

A user guide is available `here <https://cotinga.readthedocs.io/en/latest/index.html>`__.

Credits
=======

Flags are from:
https://github.com/fonttools/region-flags/
