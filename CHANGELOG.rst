Changelog
=========

Unreleased
----------


Version 0.2.5 (2022-09-21)
--------------------------

* Customize title and subtitle of the reports (see #73).

Version 0.2.4 (2022-09-10)
--------------------------

* Patch release (bugfix: wrong MIME type prevents to open saved files on Ubuntu Jammy)

Version 0.2.3 (2022-09-10)
--------------------------

* Update Generate and Merge panels to match newest mathmaker's API (0.7.17): let the user choose the year's series and provide belts names to mathmaker (via --belts option), from the user's own belts levels defined in Cotinga.

Version 0.2.2 (2022-08-25)
--------------------------
* User documentation
* Package for Ubuntu 22.04 (in addition to Ubuntu 20.04)


Version 0.2 (2022-07-06) and patch 0.2.1 (2022-07-11)
-----------------------------------------------------

* GUI reorganization.
* Add "merge tests" feature.
* Add an optional "create tests templates" feature (using mathmaker).
* Allow user to choose report's column width and whether 1 or 2 tables may be drawn per page.
* A pupil can be assigned several classes, allowing to move pupils from one class to another.
* A grade can be deleted.
* Internal code reorganization; replace toml by json for internal configuration files.
* Bug fixes.
* Package for Ubuntu (20.04).

Initial version 0.1 and patches 0.1.1, 0.1.2, 0.1.3 (2018-09-09, 2018-10-05 and 2018-10-17)
-------------------------------------------------------------------------------------------

* All basic features of Cotinga and minor improvements.
