.. Cotinga documentation master file, created by
   sphinx-quickstart on Thu Aug 11 15:48:50 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cotinga's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   disclaimer
   install
   quickstart
   usage
   developers


.. Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
