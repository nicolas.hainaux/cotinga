��          T      �       �   �   �      �     �     �     �  �   �  E  �  �                  7     H  .  h                                        Cotinga is written in ``python``, hence may work on systems where ``python`` is installed. A `python package <https://pypi.org/project/cotinga/>`__ is available , so you should be able to simply ``pip install cotinga``. Install On Ubuntu 20.04 or 22.04 Other systems There's a PPA available: Yet, as Cotinga is a GTK application, other dependencies will certainly be required for Cotinga to work. If you need inspiration for missing dependencies, you'll find a list of the required ones for Ubuntu in the ``debian/control`` file. Project-Id-Version: cotinga 0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-08-14 09:20+0200
Last-Translator: 
Language: fr
Language-Team: 
Plural-Forms: nplurals=2; plural=(n > 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.3
X-Generator: Poedit 2.3
 Cotinga est écrit en ``python``, donc devrait fonctionner sur les systèmes où ``python`` est installé. Un `paquetage python <https://pypi.org/project/cotinga/>`__ est disponible, donc vous devriez pouvoir simplement exécuter ``pip install cotinga``. Installation Sur Ubuntu 20.04 ou 22.04 Autres systèmes Un dépôt PPA est disponible : Cependant, comme Cotinga est une application GTK, d'autres dépendances seront certainement nécessaires pour que Cotinga fonctionne. Si vous avez besoin d'inspiration pour les dépendances manquantes, vous trouverez une liste des dépendances nécessaires à Ubuntu dans le fichier ``debian/control``. 