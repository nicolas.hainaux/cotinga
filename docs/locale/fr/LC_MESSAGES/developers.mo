��          \      �       �   7   �   q     
   s  U   ~  W   �  X   ,  �   �  E  )  J   o  x   �     3  �   A  a   �  s   -  �   �                                       Activate the venv where the install took place and try: Cotinga is managed by ``poetry``, what you'll need to `install <https://python-poetry.org/docs/#installation>`__. Developers Once done, get to the repo's root and run (maybe create a virtual environment first): The source code is at the `gitlab repo <https://gitlab.com/nicolas.hainaux/cotinga>`__. You'll need ``python`` installed (check minimum version required in ``pyproject.toml``). You'll need dependencies. See ``debian/control`` for Ubuntu packages (all, or most of the ones starting with ``python3-`` should be handled by ``poetry``, though). Project-Id-Version: cotinga 0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-08-14 09:24+0200
Last-Translator: 
Language: fr
Language-Team: 
Plural-Forms: nplurals=2; plural=(n > 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.3
X-Generator: Poedit 2.3
 Activez l'environnement virtuel où l'installation a eu lieu, et essayez : Cotinga est géré par ``poetry``, que vous aurez besoin `d'installer <https://python-poetry.org/docs/#installation>`__. Développeurs Une fois que c'est fait, allez à la racine du dépôt et exécutez (peut-être en ayant créé un environnement virtuel au préalable) : Le code source est accessible au `dépôt gitlab <https://gitlab.com/nicolas.hainaux/cotinga>`__. Vous aurez besoin d'avoir ``python`` installé (vérifiez la version minimale nécessaire dans ``pyproject.toml``). Vous aurez besoin de dépendances. Voir ``debian/control`` pour les paquetages Ubuntu (tous, ou la plupart de ceux commençant par ``python3-`` devraient être gérés par ``poetry``, cependant). 