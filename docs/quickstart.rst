Quickstart
==========

Create and setup a new document
-------------------------------

Once you start Cotinga, it'll display the welcome page (no document loaded).

.. image:: pics/toolbar_nodocument.png

Click on "New" and Cotinga shows the empty document page:

.. image:: pics/toolbar_emptydocument.png

Click on "Settings" to open the document setup dialog:

.. image:: pics/docsetup_classes_empty.png

There, you can add, remove and order your classes.

You can define the levels you wish:

.. image:: pics/docsetup_levels.png

the grading type (whether it is numeric or literal; the edge from where a student successfully passed a test):

.. image:: pics/docsetup_grading_numeric.png
.. image:: pics/docsetup_grading_literal.png

and the special grades (any grade that does not belong to the grades scale, but should be considered as correct value, for instance when a student was absent, or won't get a grade for a certain test etc.)

.. image:: pics/docsetup_special_grades.png

Populate your classes in the classes tab
----------------------------------------

You can insert new pupils one by one (click on "Insert a pupil"):

.. image:: pics/panel_classes_add_pupil.png

but it'll be faster to paste a list of pupils from a spreadsheet document: select the pupils' names:

.. image:: pics/spreadsheet_pupils_selection.png

and click on "Paste pupils". After a confirmation dialog, all pupils will be there:

.. image:: pics/panel_classes_pasted_pupils.png
