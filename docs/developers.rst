Developers
==========

The source code is at the `gitlab repo <https://gitlab.com/nicolas.hainaux/cotinga>`__.

You'll need ``python`` installed (check minimum version required in ``pyproject.toml``).

Cotinga is managed by ``poetry``, what you'll need to `install <https://python-poetry.org/docs/#installation>`__.

You'll need dependencies. See ``debian/control`` for Ubuntu packages (all, or most of the ones starting with ``python3-`` should be handled by ``poetry``, though).

Once done, get to the repo's root and run (maybe create a virtual environment first):

::

    poetry install

Activate the venv where the install took place and try:

::

    cotinga
