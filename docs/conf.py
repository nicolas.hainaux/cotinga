# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import toml
from pathlib import Path

# CAUTION: the builder on readthedocs does not allow to use Xfvb server (not
# easily as of august 2022) what makes impossible to import things from
# Cotinga as it would require a graphical server (Cotinga being GTK app).
# Hence, the metadata below are not imported from cotinga.core.env, as doing
# so would make the build fail on readthedocs.
METADATA = toml.loads(
    (Path(__file__).parent.parent / 'pyproject.toml').read_text())
VERSION = METADATA['tool']['poetry']['version']
AUTHORS = METADATA['tool']['poetry']['authors']

project = METADATA['tool']['poetry']['name']
author = ', '.join(AUTHORS)
copyright = f'2018-2022, {author}'
# The short X.Y version.
version = VERSION[:3]
# The full version, including alpha/beta/rc tags.
release = VERSION

language = 'en'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = []

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

# -- Options for internationalization ----------------------------------------
locale_dirs = ['locale/']   # path is example but recommended.
gettext_compact = False     # optional.
figure_language_filename = '{path}{language}/{basename}{ext}'

# -- Mock PyGObject ----------------------------------------------------------
autodoc_mock_imports = ['gi']
