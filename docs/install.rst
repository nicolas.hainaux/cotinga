Install
=======

On Ubuntu 20.04 or 22.04
------------------------

There's a PPA available:

   ::

       sudo add-apt-repository ppa:zezollo/chantli
       sudo apt update
       sudo apt install cotinga


Other systems
-------------

Cotinga is written in ``python``, hence may work on systems where ``python`` is installed. A `python package <https://pypi.org/project/cotinga/>`__ is available , so you should be able to simply ``pip install cotinga``.

Yet, as Cotinga is a GTK application, other dependencies will certainly be required for Cotinga to work. If you need inspiration for missing dependencies, you'll find a list of the required ones for Ubuntu in the ``debian/control`` file.
