# -*- coding: utf-8 -*-

# Cotinga helps maths teachers creating worksheets
# and managing pupils' progression.
# Copyright 2018-2022 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Cotinga.

# Cotinga is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Cotinga is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Cotinga; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import gi
try:
    gi.require_version('Pango', '1.0')
except ValueError:
    raise
else:
    from gi.repository import Pango

import pytest

from cotinga.core.tools import check_grade, grade_ge_edge, cellfont_fmt
from cotinga.core.tools import calculate_attained_level, Listing, file_uri

NUMERIC_GRADING = {"choice": "numeric",
                   "edge_literal": "B", "edge_numeric": 15,
                   "literal_grades": ["A", "B", "C", "D", "E"],
                   "maximum": 20, "minimum": 0, "step": 1}

NUMERIC_GRADING2 = {"choice": "numeric",
                    "edge_literal": "B", "edge_numeric": 15,
                    "literal_grades": ["A", "B", "C", "D", "E"],
                    "maximum": 20, "minimum": 0, "step": 0}

LITERAL_GRADING = {"choice": "literal",
                   "edge_literal": "B", "edge_numeric": 15,
                   "literal_grades": ["A", "B", "C", "D", "E"],
                   "maximum": 20, "minimum": 0, "step": 1}

SPECIAL_GRADES = ["ABS", "NN"]

LEVELS = ['L0', 'L1', 'L2', 'L3', 'L4', 'L5', 'L6']


def test_check_grade():
    assert check_grade("ABS", SPECIAL_GRADES, NUMERIC_GRADING) == (True, False)
    assert check_grade("", SPECIAL_GRADES, NUMERIC_GRADING) == (False, False)
    assert check_grade(None, SPECIAL_GRADES, NUMERIC_GRADING) == (False, False)
    assert check_grade('abc', SPECIAL_GRADES, NUMERIC_GRADING) \
        == (False, False)
    assert check_grade('24', SPECIAL_GRADES, NUMERIC_GRADING) == (False, False)
    assert check_grade('16', SPECIAL_GRADES, NUMERIC_GRADING) == (True, True)
    assert check_grade('15.25', SPECIAL_GRADES, NUMERIC_GRADING) \
        == (False, False)
    assert check_grade('15,5', SPECIAL_GRADES, NUMERIC_GRADING) == (True, True)
    assert check_grade('15,5', SPECIAL_GRADES, NUMERIC_GRADING2) \
        == (False, False)
    assert check_grade('15,5', SPECIAL_GRADES, LITERAL_GRADING) \
        == (False, False)
    assert check_grade('F', SPECIAL_GRADES, LITERAL_GRADING) == (False, False)
    assert check_grade('B', SPECIAL_GRADES, LITERAL_GRADING) == (True, True)


def test_grade_ge_edge():
    assert grade_ge_edge(NUMERIC_GRADING, '17', SPECIAL_GRADES)
    assert grade_ge_edge(NUMERIC_GRADING, '17.5', SPECIAL_GRADES)
    assert grade_ge_edge(NUMERIC_GRADING, '17,5', SPECIAL_GRADES)
    assert grade_ge_edge(NUMERIC_GRADING, 17, SPECIAL_GRADES)
    assert grade_ge_edge(NUMERIC_GRADING, 17.5, SPECIAL_GRADES)
    assert not grade_ge_edge(NUMERIC_GRADING, 17.25, SPECIAL_GRADES)
    assert not grade_ge_edge(NUMERIC_GRADING, 14, SPECIAL_GRADES)
    assert not grade_ge_edge(NUMERIC_GRADING, 'ABS', SPECIAL_GRADES)
    assert grade_ge_edge(LITERAL_GRADING, 'A', SPECIAL_GRADES)
    assert grade_ge_edge(LITERAL_GRADING, 'B', SPECIAL_GRADES)
    assert not grade_ge_edge(LITERAL_GRADING, 'C', SPECIAL_GRADES)


def test_calculate_attained_level():
    assert calculate_attained_level('L0', LEVELS, NUMERIC_GRADING,
                                    ['18', ],
                                    SPECIAL_GRADES) == 'L1'
    assert calculate_attained_level('L0', LEVELS, NUMERIC_GRADING,
                                    [18, 19, 20, 16],
                                    SPECIAL_GRADES) == 'L4'
    assert calculate_attained_level('L0', LEVELS, NUMERIC_GRADING,
                                    [18, 19, 'ABS', 16],
                                    SPECIAL_GRADES) == 'L3'
    assert calculate_attained_level('L0', LEVELS, NUMERIC_GRADING,
                                    [18, 19, 'ABS', 'abc'],
                                    SPECIAL_GRADES) == 'L2'
    assert calculate_attained_level('L0', LEVELS, LITERAL_GRADING,
                                    ['A', 'B', 'ABS', 'C'],
                                    SPECIAL_GRADES) == 'L2'


def test_cellfont_fmt():
    assert cellfont_fmt("ABS", SPECIAL_GRADES, NUMERIC_GRADING) \
        == ('Grey', int(Pango.Weight.NORMAL))
    assert cellfont_fmt(16, SPECIAL_GRADES, NUMERIC_GRADING) \
        == ('Black', int(Pango.Weight.NORMAL))
    assert cellfont_fmt("ABSENT", SPECIAL_GRADES, NUMERIC_GRADING) \
        == ('Firebrick', int(Pango.Weight.BOLD))


def test_Listing_instanciation():
    with pytest.raises(TypeError) as excinfo:
        Listing(None, data_row=6)
    assert str(excinfo.value) == 'Argument data_row should be a list, '\
        'found <class \'int\'> instead'
    L = Listing(0)
    assert L.cols == [0, ]
    L = Listing(None)
    assert L.cols == []
    L = Listing([0, 1, 2], data_row=['a', 'b', 'c'])
    assert L.cols == ['a', 'b', 'c', 0, 1, 2]
    L = Listing(18, data_row=[10, 12, 14, 16], position=2)
    assert L.cols == [10, 12, 18, 16]


def test_file_uri():
    assert file_uri('/some/path') == 'file:///some/path'
