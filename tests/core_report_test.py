# -*- coding: utf-8 -*-

# Cotinga helps maths teachers creating worksheets
# and managing pupils' progression.
# Copyright 2018-2022 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Cotinga.

# Cotinga is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Cotinga is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Cotinga; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

from pathlib import Path
from shutil import copyfile

import pytest
from reportlab.platypus import Table, Paragraph, PageBreak, Spacer

from cotinga.core.env import TESTDIR_DATA, RUN_DOCSETUP, DEFAULT_DOCSETUP
from cotinga.core.report import rework_data, workout_spans
from cotinga.core.report import slice_table, split_table, split_spans
from cotinga.core.report import build_tables, collate_elements
from cotinga.core.report import determine_max_cols, fix_columns_titles
from cotinga.core.report import build, split, splitname, cleanup
from cotinga.models import Pupils

SALOUA = \
    Pupils(included=True, classnames=['6A'], fullname='ELAMI Saloua',
           initial_level='Ceinture blanche', attained_level='Ceinture blanche',
           grades=['ABS', 'ABS', 'ABS', 'ABS', 'NE'])

MEMET = \
    Pupils(included=True, classnames=['6A', 'AP'], fullname='BOZGURT Memet',
           initial_level='Ceinture blanche',
           attained_level='Ceinture blanche I',
           grades=['18', '11', '11', '13', '11'])

LAIA = \
    Pupils(included=True, classnames=['6A', 'AP'], fullname='SANCHEZ Laia',
           initial_level='Ceinture blanche',
           attained_level='Ceinture blanche I',
           grades=['20', '6', '4', '8', '10'])

JULIETTE = \
    Pupils(included=True, classnames=['6A'], fullname='COMBALLES Juliette',
           initial_level='Ceinture blanche',
           attained_level='Ceinture blanche I',
           grades=['16', '14', '14', '10', '14'])

THOMAS = \
    Pupils(included=True, classnames=['6A', 'AP'], fullname='DURAND Thomas',
           initial_level='Ceinture blanche',
           attained_level='Ceinture blanche II',
           grades=['10', '18', '10,5', '14', '16'])

MALENA = \
    Pupils(included=True, classnames=['6A'], fullname='JIMENEZ Malena',
           initial_level='Ceinture blanche',
           attained_level='Ceinture blanche II',
           grades=['17', '15', '13', '11', '14'])

RAW_DATA1 = [('Ceinture blanche I', 1, [SALOUA]),
             ('Ceinture blanche II', 3, [MEMET, LAIA, JULIETTE]),
             ('Ceinture jaune', 2, [THOMAS, MALENA])]

WARDA = \
    Pupils(included=True, classnames=['6A', 'AP'], fullname='RAHIM Warda',
           initial_level='Ceinture blanche', attained_level='Ceinture jaune',
           grades=['13', '14', '16', '16,5', '15'])

JEAN = \
    Pupils(included=True, classnames=['6A', 'AP'], fullname='COURNAT Jean',
           initial_level='Ceinture blanche', attained_level='Ceinture jaune',
           grades=['17', '16', '16', '13,5', '14,5'])

GABY = \
    Pupils(included=True, classnames=['6A'], fullname='RAMPAL Gaby',
           initial_level='Ceinture blanche', attained_level='Ceinture jaune',
           grades=['16', '14,5', '18,5', '9,5', '18'])

ANTOINE = \
    Pupils(included=True, classnames=['6A', 'AP'], fullname='DENIS Antoine',
           initial_level='Ceinture blanche', attained_level='Ceinture jaune',
           grades=['19', '17,5', '12', '11', '16'])

MASSIM = \
    Pupils(included=True, classnames=['6A', 'AP'], fullname='MOHAMEDI Massim',
           initial_level='Ceinture blanche', attained_level='Ceinture jaune',
           grades=['19', '12', '16', '16', '14'])

PEDRO = \
    Pupils(included=True, classnames=['6A'], fullname='MARTINEZ Pedro',
           initial_level='Ceinture blanche', attained_level='Ceinture jaune',
           grades=['20', '19', '16', '12,5', '13,5'])

MARGOT = \
    Pupils(included=True, classnames=['6A'], fullname='PERRIN Margot',
           initial_level='Ceinture blanche', attained_level='Ceinture jaune',
           grades=['11', '18', '18', '14,5', '15'])


RAW_DATA2 = [('Ceinture blanche I', 1, [SALOUA]),
             ('Ceinture blanche II', 3, [MEMET, LAIA, JULIETTE]),
             ('Ceinture jaune', 2, [THOMAS, MALENA]),
             ('Ceinture jaune I', 7, [WARDA, JEAN, GABY, ANTOINE, MASSIM,
                                      PEDRO, MARGOT])]


RAW_DATA3 = [('Ceinture blanche I', 3, [SALOUA, MEMET, LAIA]),
             ('Ceinture blanche II', 1, [JULIETTE]),
             ('Ceinture jaune', 2, [THOMAS, MALENA]),
             ('Ceinture jaune I', 7, [WARDA, JEAN, GABY, ANTOINE, MASSIM,
                                      PEDRO, MARGOT])]

REWORKED1 = [
    ['Ceinture blanche I (1)', 'Ceinture blanche II (3)',
     'Ceinture jaune (2)'],
    ['ELAMI Saloua', 'BOZGURT Memet', 'DURAND Thomas'],
    ['', 'COMBALLES Juliette', 'JIMENEZ Malena'],
    ['', 'SANCHEZ Laia', '']]

REWORKED2_1 = [
    ['Ceinture blanche I (1)', 'Ceinture blanche II (3)',
     'Ceinture jaune (2)', 'Ceinture jaune I (7)'],
    ['ELAMI Saloua', 'BOZGURT Memet', 'DURAND Thomas', 'COURNAT Jean'],
    ['', 'COMBALLES Juliette', 'JIMENEZ Malena', 'DENIS Antoine'],
    ['', 'SANCHEZ Laia', '', 'MARTINEZ Pedro'],
    ['', '', '', 'MOHAMEDI Massim'],
    ['', '', '', 'PERRIN Margot'],
    ['', '', '', 'RAHIM Warda'],
    ['', '', '', 'RAMPAL Gaby']
]


def test_determine_max_cols():
    with pytest.raises(ValueError) as excinfo:
        determine_max_cols('a')
    assert str(excinfo.value).startswith('could not convert string to float')
    with pytest.raises(ValueError) as excinfo:
        determine_max_cols(3.69)
    assert str(excinfo.value).startswith('width is assumed to be comprised '
                                         'between 3.7 and 7.4')
    with pytest.raises(ValueError) as excinfo:
        determine_max_cols(7.41)
    assert str(excinfo.value).startswith('width is assumed to be comprised '
                                         'between 3.7 and 7.4')
    assert determine_max_cols(3.7) == 7
    assert determine_max_cols(4.2) == 6
    assert determine_max_cols(4.9) == 5
    assert determine_max_cols(5.9) == 4
    assert determine_max_cols(7.4) == 4


def test_slice_table_errors():
    with pytest.raises(ValueError) as excinfo:
        slice_table([], 'a')
    assert str(excinfo.value).startswith(
        'invalid literal for int() with base 10:')
    with pytest.raises(ValueError) as excinfo:
        slice_table([], -5)
    assert str(excinfo.value).startswith(
        'argument n is assumed to be positive')


def test_slice_table():
    assert slice_table([], 1) == ([], [])
    assert slice_table(REWORKED1, 0) == (
        [['Ceinture blanche I (1)'], ['ELAMI Saloua'], [''], ['']],
        [['Ceinture blanche II (3)', 'Ceinture jaune (2)'],
         ['BOZGURT Memet', 'DURAND Thomas'],
         ['COMBALLES Juliette', 'JIMENEZ Malena'],
         ['SANCHEZ Laia', '']]
    )
    assert slice_table(REWORKED1, 1) == (
        [['Ceinture blanche I (1)', 'Ceinture blanche II (3)'],
         ['ELAMI Saloua', 'BOZGURT Memet'], ['', 'COMBALLES Juliette'],
         ['', 'SANCHEZ Laia']],
        [['Ceinture jaune (2)'], ['DURAND Thomas'], ['JIMENEZ Malena'], ['']]
    )
    assert slice_table(REWORKED1, 2) == (
        [['Ceinture blanche I (1)', 'Ceinture blanche II (3)',
          'Ceinture jaune (2)'],
         ['ELAMI Saloua', 'BOZGURT Memet', 'DURAND Thomas'],
         ['', 'COMBALLES Juliette', 'JIMENEZ Malena'],
         ['', 'SANCHEZ Laia', '']],
        []
    )
    assert slice_table(REWORKED1, 3) == (
        [['Ceinture blanche I (1)', 'Ceinture blanche II (3)',
          'Ceinture jaune (2)'],
         ['ELAMI Saloua', 'BOZGURT Memet', 'DURAND Thomas'],
         ['', 'COMBALLES Juliette', 'JIMENEZ Malena'],
         ['', 'SANCHEZ Laia', '']],
        []
    )


def test_split_table():
    assert split_table(REWORKED2_1, 2) == \
        [[['Ceinture blanche I (1)', 'Ceinture blanche II (3)'],
          ['ELAMI Saloua', 'BOZGURT Memet'],
          ['', 'COMBALLES Juliette'],
          ['', 'SANCHEZ Laia'],
          ['', ''],
          ['', ''],
          ['', ''],
          ['', '']],
         [['Ceinture jaune (2)', 'Ceinture jaune I (7)'],
          ['DURAND Thomas', 'COURNAT Jean'],
          ['JIMENEZ Malena', 'DENIS Antoine'],
          ['', 'MARTINEZ Pedro'],
          ['', 'MOHAMEDI Massim'],
          ['', 'PERRIN Margot'],
          ['', 'RAHIM Warda'],
          ['', 'RAMPAL Gaby']]]

    assert split_table(REWORKED2_1, 3) == \
        [[['Ceinture blanche I (1)', 'Ceinture blanche II (3)',
           'Ceinture jaune (2)'],
          ['ELAMI Saloua', 'BOZGURT Memet', 'DURAND Thomas'],
          ['', 'COMBALLES Juliette', 'JIMENEZ Malena'],
          ['', 'SANCHEZ Laia', ''],
          ['', '', ''],
          ['', '', ''],
          ['', '', ''],
          ['', '', '']],
         [['Ceinture jaune I (7)'],
          ['COURNAT Jean'],
          ['DENIS Antoine'],
          ['MARTINEZ Pedro'],
          ['MOHAMEDI Massim'],
          ['PERRIN Margot'],
          ['RAHIM Warda'],
          ['RAMPAL Gaby']]]


def test_rework_data():
    data, spans, classes = rework_data(RAW_DATA1, 10)
    assert spans == [False, False, False, False]
    assert classes == {'6A', 'AP'}
    assert data == REWORKED1

    data, spans, classes = rework_data(RAW_DATA2, 10)
    assert spans == [False, False, False, False, False]
    assert classes == {'6A', 'AP'}
    assert data == REWORKED2_1

    data, spans, classes = rework_data(RAW_DATA2, 5)
    assert spans == [False, False, False, False, True, False]
    assert classes == {'6A', 'AP'}
    assert data == [
        ['Ceinture blanche I (1)', 'Ceinture blanche II (3)',
         'Ceinture jaune (2)', 'Ceinture jaune I (7)', ''],
        ['ELAMI Saloua', 'BOZGURT Memet', 'DURAND Thomas', 'COURNAT Jean',
         'RAHIM Warda'],
        ['', 'COMBALLES Juliette', 'JIMENEZ Malena', 'DENIS Antoine',
         'RAMPAL Gaby'],
        ['', 'SANCHEZ Laia', '', 'MARTINEZ Pedro', ''],
        ['', '', '', 'MOHAMEDI Massim', ''],
        ['', '', '', 'PERRIN Margot', '']]

    data, spans, classes = rework_data(RAW_DATA2, 2)
    assert spans == [False, False, True, False, False, True, True, True, False]
    assert classes == {'6A', 'AP'}
    assert data == [
        ['Ceinture blanche I (1)', 'Ceinture blanche II (3)', '',
         'Ceinture jaune (2)', 'Ceinture jaune I (7)', '', '', ''],
        ['ELAMI Saloua', 'BOZGURT Memet', 'SANCHEZ Laia', 'DURAND Thomas',
         'COURNAT Jean', 'MARTINEZ Pedro', 'PERRIN Margot', 'RAMPAL Gaby'],
        ['', 'COMBALLES Juliette', '', 'JIMENEZ Malena', 'DENIS Antoine',
         'MOHAMEDI Massim', 'RAHIM Warda', '']]

    data, spans, classes = rework_data(RAW_DATA3, 2)
    assert spans == [False, True, False, False, False, True, True, True, False]
    assert classes == {'6A', 'AP'}
    assert data == [
        ['Ceinture blanche I (3)', '', 'Ceinture blanche II (1)',
         'Ceinture jaune (2)', 'Ceinture jaune I (7)', '', '', ''],
        ['BOZGURT Memet', 'SANCHEZ Laia', 'COMBALLES Juliette',
         'DURAND Thomas', 'COURNAT Jean', 'MARTINEZ Pedro', 'PERRIN Margot',
         'RAMPAL Gaby'],
        ['ELAMI Saloua', '', '', 'JIMENEZ Malena', 'DENIS Antoine',
         'MOHAMEDI Massim', 'RAHIM Warda', '']]


def test_workout_spans():
    assert workout_spans([False, False, False]) == []
    assert workout_spans([False, False, False, False, True, False]) \
        == [('SPAN', (3, 0), (4, 0))]
    assert workout_spans([False, True, False]) == [('SPAN', (0, 0), (1, 0))]
    assert workout_spans([False, True, False, True, True, False]) \
        == [('SPAN', (0, 0), (1, 0)), ('SPAN', (2, 0), (4, 0))]


def test_split_spans():
    assert split_spans([False, False, False, False, False, False], 3) \
        == [[False, False, False, False],
            [False, False, False]]
    assert split_spans([False, False, True, True, False, False], 3) \
        == [[False, False, True, False],
            [False, False, False]]
    assert split_spans([False, False, False], 3) \
        == [[False, False, False]]
    assert split_spans([False, False, False], 2) \
        == [[False, False, False]]


def test_build_tables():
    colwidth = 4.8
    max_cols = 6
    data, spans, _ = rework_data(RAW_DATA1, 10)
    tables = build_tables(data, spans, max_cols, colwidth)
    assert len(tables) == 1

    colwidth = 7
    max_cols = 4
    data, spans, _ = rework_data(RAW_DATA2, 2)
    tables = build_tables(data, spans, max_cols, colwidth)
    assert len(tables) == 2


def test_collate_elements():
    classes = '6A'
    date_fmt = '%Y-%m-%d'
    colwidth = 7
    max_cols = 4
    data, spans, _ = rework_data(RAW_DATA2, 2)  # set max_rows to 2
    tables = build_tables(data, spans, max_cols, colwidth)
    pages_nb, elts = collate_elements('title', 'subtitle', classes, date_fmt,
                                      1, tables)
    assert pages_nb == 2
    assert len(elts) == 5
    assert type(elts[0]) == Paragraph
    assert type(elts[1]) == Paragraph
    assert type(elts[2]) == Table
    assert type(elts[3]) == PageBreak
    assert type(elts[4]) == Table
    pages_nb, elts = collate_elements('title', 'subtitle', classes, date_fmt,
                                      2, tables)
    assert pages_nb == 1
    assert len(elts) == 5
    assert type(elts[0]) == Paragraph
    assert type(elts[1]) == Paragraph
    assert type(elts[2]) == Table
    assert type(elts[3]) == Spacer
    assert type(elts[4]) == Table


def test_fix_columns_titles():
    titles_to_fix = [
        [['Ceinture blanche I (3)', '', 'Ceinture blanche II (3)'],
         ['ELAMI Saloua', 'COMBALLES Juliette', 'DURAND Thomas'],
         ['BOZGURT Memet', '', 'JIMENEZ Malena']],
        [['', 'Ceinture jaune (2)'],
         ['MOHAMEDI Massim', 'SANCHEZ Laia'],
         ['', 'PERRIN Margot']]]
    fix_columns_titles(titles_to_fix)
    assert titles_to_fix[1][0][0] == 'Ceinture blanche II (3)'
    assert titles_to_fix[0][0][1] == ''


def test_build(mocker):
    if not RUN_DOCSETUP.is_file():
        RUN_DOCSETUP.parent.mkdir(parents=True, exist_ok=True)
        copyfile(DEFAULT_DOCSETUP['en_US'], RUN_DOCSETUP)
    # do not really create the pdf file
    mocker.patch('cotinga.core.report.SimpleDocTemplate.build')
    # only check no exception is raised and page nb is 1
    assert build(RAW_DATA2) == 1


def test_splitname():
    assert splitname(TESTDIR_DATA / 'fourpages.pdf', 4) \
        == TESTDIR_DATA / 'fourpages4.pdf'


def test_split_and_cleanup():
    split(TESTDIR_DATA / 'fourpages.pdf')
    assert Path(TESTDIR_DATA / 'fourpages0.pdf').is_file()
    assert Path(TESTDIR_DATA / 'fourpages1.pdf').is_file()
    assert Path(TESTDIR_DATA / 'fourpages2.pdf').is_file()
    assert Path(TESTDIR_DATA / 'fourpages3.pdf').is_file()
    cleanup(TESTDIR_DATA / 'fourpages.pdf')
    assert not Path(TESTDIR_DATA / 'fourpages0.pdf').is_file()
    assert not Path(TESTDIR_DATA / 'fourpages1.pdf').is_file()
    assert not Path(TESTDIR_DATA / 'fourpages2.pdf').is_file()
    assert not Path(TESTDIR_DATA / 'fourpages3.pdf').is_file()
